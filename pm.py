#!/usr/bin/env python3


import os


PROJECTS = [
        "blackjack",
        "hilo",
        "mangawae",
        "Pacmen",
        "pm",
        "website"
        ]
PROJECT_DIRECTORY = "/home/mdp/prod"
GROUP_URL = "git@gitlab.com:dao14"
GIT_ACTIONS = [
        "add",
        "clone",
        "commit",
        "pull",
        "push",
        ]


class storage :

    def get_installed_projects( PROJECT_DIRECTORY ) :
        try :
            return os.listdir( PROJECT_DIRECTORY )
        except FileNotFoundError :
            return False

    def initialize_directory( PROJECT_DIRECTORY ) :
        os.system( "mkdir -p %s" % PROJECT_DIRECTORY )


class git :

    def add( PROJECT_DIRECTORY, project ) :
        os.system( "cd %s/%s && git add -A" % ( PROJECT_DIRECTORY, project ) )
    def clone( PROJECT_DIRECTORY, GROUP_URL, project ) :
        os.system( "cd %s && git clone %s/%s.git" % ( PROJECT_DIRECTORY, GROUP_URL, project ) )
    def commit( PROJECT_DIRECTORY, project, message ) :
        os.system( "cd %s/%s && git commit -a -m '%s" % ( PROJECTDIRECTORY, project, message ) )
    def pull( PROJECT_DIRECTORY, project ) :
        os.system( "cd %s/%s && git pull" % ( PROJECT_DIRECTORY, project ) )
    def push( PROJECT_DIRECTORY, project ) :
        os.system( "cd %s/%s && git push" % ( PROJECT_DIRECTORY, project ) )


def main( PROJECT_DIRECTORY, GROUP_URL, GIT_ACTIONS ) :

    storage.initialize_directory( PROJECT_DIRECTORY )

    #  Missing project logic
    installed_projects = storage.get_installed_projects( PROJECT_DIRECTORY )
    for project in PROJECTS :
        #  Track if the project is installed, if not install
        installed = False
        if not installed_projects :
            git.clone( PROJECT_DIRECTORY, GROUP_URL, project )
            installed = True
        else :
            for installed_project in installed_projects :
                if installed_project == project :
                    installed = True
        if not installed :
            git.clone( PROJECT_DIRECTORY, GROUP_URL, project )
            installed = True

    #  Project input
    correct = False
    while not correct :

        for i, project in enumerate( PROJECTS ) :
            print( ">> %s > %s" % ( i, project ) )

        project = input( ">> " )
        for p in PROJECTS :
            if project == p :
                correct = True

    correct = False
    while not correct :
        for i, action in enumerate( GIT_ACTIONS ) :
            print( ">> %s > %s" % ( i, action ) )

        action = input( ">> " )
        for a in GIT_ACTIONS :
            if action == a :
                correct = True

    if action == "add" :
        git.add( PROJECT_DIRECTORY, project )
    elif action == "clone" :
        git.clone( PROJECT_DIRECTORY, GROUP_URL, project )
    elif action == "commit" :
        message = input( ">> commit message > " )
        git.commit( PROJECT_DIRECTORY, project, message )
    elif action == "pull" :
        git.pull( PROJECT_DIRECTORY, project )
    elif action == "push" :
        git.push( PROJECT_DIRECTORY, project )


if __name__ == "__main__" :
    main( PROJECT_DIRECTORY, GROUP_URL, GIT_ACTIONS )
